/* ********************************************************************************** */
/*                                                                                    */
/*                                  MCMC Samplers                                     */
/*                                                                                    */
/* ********************************************************************************** */

void RJMCMC(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct Chain *chain, struct Prior *prior, double *logEvidence, double *varLogEvidence);
void PTMCMC(struct Model **model, struct Chain *chain, gsl_rng *seed, int NC);

void EvolveBayesLineParameters(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct Chain *chain, struct Prior *prior, int ic);
void EvolveIntrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, struct TimeFrequencyMap *tf, gsl_rng *seed, int ic);
void EvolveExtrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, gsl_rng *seed, int ic);

/* ********************************************************************************** */
/*                                                                                    */
/*                                    MCMC tools                                      */
/*                                                                                    */
/* ********************************************************************************** */

void adapt_temperature_ladder(struct Chain *chain, int NC);
