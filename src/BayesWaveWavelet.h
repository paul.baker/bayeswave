/* ********************************************************************************** */
/*                                                                                    */
/*                            Wavelet basis functions                                 */
/*                                                                                    */
/* ********************************************************************************** */

double SineGaussianSNR(double *params, double *Snf, double Tobs);
void SineGaussianBandwidth(double *sigpar, double *fmin, double *fmax);
void SineGaussianFourier(double *hs, double *sigpar, int N, int flag, double Tobs);
void SineGaussianFisher(double *params, struct FisherMatrix *fisher);

double ChirpletSNR(double *params, double *Snf, double Tobs);
void ChirpletBandwidth(double *sigpar, double *fmin, double *fmax);
void ChirpletFourier(double *hs, double *sigpar, int N, int flag, double Tobs);
void ChirpletFisher(double *params, struct FisherMatrix *fisher);

/* ********************************************************************************** */
/*                                                                                    */
/*                         Network projection functions                               */
/*                                                                                    */
/* ********************************************************************************** */

void computeProjectionCoeffs(struct Data *data, struct Network *projection, double *extParams, double fmin, double fmax);
void waveformProject(struct Data *data, struct Network *projection, double *extParams, double **response, double **h, double fmin, double fmax);
void combinePolarizations(struct Data *data, struct Wavelet **geo, double **h, double *extParams, int Npol);
void compute_reconstructed_plus_and_cross(struct Data *data, struct Network *projection, double *extParams, double **hrecPlus, double **hrecCross, double *geo, double fmin, double fmax);
