#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h" 

/* ********************************************************************************** */
/*                                                                                    */
/*                                    Math tools                                      */
/*                                                                                    */
/* ********************************************************************************** */

double gaussian_norm(double min, double max, double sigma)
{
   double sqp2 = 1.2533141373155; //sqrt(pi/2)
   return sqp2*sigma*(erfc(min/LAL_SQRT2/sigma) - erfc(max/LAL_SQRT2/sigma) );
}

void recursive_phase_evolution(double dre, double dim, double *cosPhase, double *sinPhase)
{
   /* Update re and im for the next iteration. */
   double cosphi = *cosPhase;
   double sinphi = *sinPhase;

   double newRe = cosphi + cosphi*dre - sinphi*dim;
   double newIm = sinphi + cosphi*dim + sinphi*dre;

   *cosPhase = newRe;
   *sinPhase = newIm;
   
}

double fourier_nwip(int imin, int imax, double *a, double *b, double *invSn)
{
    int i, j, k;
    double arg;
    /*
    double product;
    double ReA, ReB, ImA, ImB;
     */

    arg = 0.0;
    for(i=imin; i<imax; i++)
    {
        j = i * 2;
        k = j + 1;
        /*
         ReA = a[j]; ImA = a[k];
         ReB = b[j]; ImB = b[k];
         product = ReA*ReB + ImA*ImB;
         arg += product*invSn[i];
         */
        arg += (a[j]*b[j] + a[k]*b[k])*invSn[i];
    }
    return(2.0*arg);    
}

double network_nwip(int imin, int imax, double **a, double **b, double **invSn, double *eta, int NI)
{
   int i;
   double sum=0.0;

   for(i=0; i<NI; i++) sum += fourier_nwip(imin,imax,a[i],b[i],invSn[i])/eta[i];
   return(sum);
}

double network_snr(int imin, int imax, double **h, double **invpsd, double *eta, int NI )
{
   return sqrt( network_nwip(imin,imax,h,h,invpsd,eta,NI) );
}

double detector_snr(int imin, int imax, double *g, double *invpsd, double eta)
{
   return sqrt( fourier_nwip(imin,imax,g,g,invpsd)/eta );
}

/* ********************************************************************************** */
/*                   																					                        */
/*                                  Matrix Routines                                   */
/*											                                                              */
/* ********************************************************************************** */

void matrix_eigenstuff(double **matrix, double **evector, double *evalue, int N)
{
  int i,j;

  // Don't let errors kill the program (yikes)
  gsl_set_error_handler_off ();
  int err=0;

  // Find eigenvectors and eigenvalues
  gsl_matrix *GSLfisher = gsl_matrix_alloc(N,N);
  gsl_matrix *GSLcovari = gsl_matrix_alloc(N,N);
  gsl_matrix *GSLevectr = gsl_matrix_alloc(N,N);
  gsl_vector *GSLevalue = gsl_vector_alloc(N);

  for(i=0; i<N; i++)for(j=0; j<N; j++) gsl_matrix_set(GSLfisher,i,j,matrix[i][j]);

  // sort and put them into evec
  gsl_eigen_symmv_workspace * workspace = gsl_eigen_symmv_alloc (N);
  gsl_permutation * permutation = gsl_permutation_alloc(N);
  err += gsl_eigen_symmv (GSLfisher, GSLevalue, GSLevectr, workspace);
  err += gsl_eigen_symmv_sort (GSLevalue, GSLevectr, GSL_EIGEN_SORT_ABS_ASC);
  err += gsl_linalg_LU_decomp(GSLfisher, permutation, &i);
  err += gsl_linalg_LU_invert(GSLfisher, permutation, GSLcovari);

  if(err>0)
  {
    fprintf(stderr,"BayesWaveMath.c:135: WARNING: singluar matrix, treating matrix as diagonal\n");
    fflush(stderr);
    for(i=0; i<N; i++)for(j=0; j<N; j++)
    {
      evector[i][j] = 0.0;
      if(i==j)
      {
        evector[i][j]=1.0;
        evalue[i]=matrix[i][j];
      }
    }

  }
  else
  {

    //unpack arrays from gsl inversion
    for(i=0; i<N; i++)
    {
      evalue[i] = gsl_vector_get(GSLevalue,i);
      for(j=0; j<N; j++) evector[i][j] = gsl_matrix_get(GSLevectr,i,j);
    }

    for(i=0;i<N-1;i++)for(j=i+1;j<N;j++) gsl_matrix_set(GSLcovari,j,i, gsl_matrix_get(GSLcovari,i,j) );

    //cap minimum size eigenvalues
    for(i=0; i<N; i++) if(evalue[i] < 10.) evalue[i] = 10.;
  }

  gsl_vector_free (GSLevalue);
  gsl_matrix_free (GSLfisher);
  gsl_matrix_free (GSLcovari);
  gsl_matrix_free (GSLevectr);
  gsl_eigen_symmv_free (workspace);
  gsl_permutation_free (permutation);
}

double matrix_jacobian(double **matrix, int N)
{
  int i,j;
  double detJ;

  gsl_matrix *J = gsl_matrix_alloc(N,N);

  for(i=0; i<N; i++) for(j=0; j<N; j++) gsl_matrix_set(J,i,j,matrix[i][j]);

  // get determinant of Jacobian
  gsl_permutation * permutation = gsl_permutation_alloc(N);
  gsl_linalg_LU_decomp(J, permutation, &i);

  detJ = gsl_linalg_LU_det(J, i);

  gsl_matrix_free(J);
  gsl_permutation_free (permutation);
  
  return(detJ);
}

void matrix_multiply(double **matrix, double *vector, double *result, int N)
{
  int i,j;
  for(i=0; i<N; i++)
  {
    result[i] = 0.0;
    for(j=0; j<N; j++)
    {
      result[i] += matrix[i][j]*vector[j];
    }
  }
}

double dot_product(double *a, double *b, int N)
{
  int i;
  double c = 0.0;
  for(i=0; i<N; i++) c+=a[i]*b[i];
  return c;
}

/* ********************************************************************************** */
/*																					                                          */
/*                                       RNGS                                         */
/*											                                                              */
/* ********************************************************************************** */

double gaussian_draw(gsl_rng *seed)
{
  return gsl_ran_gaussian(seed,1);
}

double uniform_draw(gsl_rng *seed)
{
  return gsl_rng_uniform(seed);
}
