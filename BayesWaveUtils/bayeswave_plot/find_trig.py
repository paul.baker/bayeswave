import numpy as np
import os

try:
    import glue.ligolw.utils
    import glue.ligolw.table
    import glue.ligolw.lsctables
except:
    print "WARNING: glue modules not found.  Will not be able to read XML tables"


filename = '/home/jkanner/baysewave/svn/trunk/burstinj/s6/BurstMDC-BRST_S6-Log.txt'

# -- Helper function for removing triggers outside a trigger list
def find_in_segs(triglist, segs):
    indx = np.zeros(len(triglist), dtype=bool)
    for n in range(len(triglist)):
        gps = triglist[n]
        indx[n] = np.any( np.logical_and( gps>(segs['start']+8), gps<(segs['stop']-4)) )
    return indx


# -- Define MDC loader function
class Mdc:

    def __init__(self, filename=filename):

        # -- Try to figure out if its an XML table
        junk, fileExtension = os.path.splitext(filename)
        
        # -- Read a CBC XML table
        if fileExtension == '.xml':
            print "READING CBC XML TABLE"
            xmldoc = glue.ligolw.utils.load_filename(filename, verbose=False)
            injs = glue.ligolw.table.get_table(xmldoc,glue.ligolw.lsctables.SimInspiralTable.tableName)

            gpsList = np.zeros(len(injs))
            longList = np.zeros(len(injs))
            latList = np.zeros(len(injs))
            indx = 0
            for inj in injs:
                gpsList[indx] = inj.h_end_time+1.e-9*inj.h_end_time_ns
                longList[indx] = inj.longitude
                latList[indx] = inj.latitude
                indx += 1
            
            self.dec = latList
            self.gps = gpsList
            self.phi = latList
            self.theta = np.pi/2 - self.dec
            

        # -- Read a Burst Log file
        else:
            namestring = """
 GravEn_SimID                        SimHrss      SimEgwR2    GravEn_Ampl  Internal_x   Internal_phi   External_x   External_phi  External_psi  FrameGPS     EarthCtrGPS     SimName    
           SimHpHp      SimHcHc      SimHpHc    GEO      GEOctrGPS       GEOfPlus       GEOfCross  H1       H1ctrGPS        H1fPlus        H1fCross  H2       H2ctrGPS        H2fPlus     
   H2fCross  L1       L1ctrGPS        L1fPlus        L1fCross  V1       V1ctrGPS        V1fPlus        V1fCross 
"""
            
            # -- Try to find name string in MDC file
            mdclog = open(filename, 'r')
            for line in mdclog:
                spl = line.split()
                if len(spl) != 0: # Ignore empty lines
                    if spl[1] == 'GravEn_SimID':
                        namestring = line[1:]
                        break
            mdclog.close()

            namelist = namestring.split()
            print namelist
            data = np.recfromtxt(filename, names=namelist)
            self.waveform = data['SimName']
            self.hrss = data['SimHrss']
            self.phi = data['External_phi']
            self.x = data['External_x']
            self.theta = np.arccos(self.x)
            self.dec = np.pi/2 - self.theta
            self.gps = data['EarthCtrGPS']
            self.ecc = data['Internal_x']

    def get_theta_phi(self, time):        
        print "Injection times:" 
        print self.gps
        print "Trying to find this time: {0}".format(time)
        n = np.where(np.abs(self.gps-time) < 0.1)
        return (self.theta[n], self.phi[n])

    def get_ecc(self, time):        
        n = np.where(np.abs(self.gps-time) < 0.1)
        return (self.ecc[n])
