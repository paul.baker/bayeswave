#!/usr/bin/env python
import lal
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
import sys
from lal import GreenwichSiderealTime
from bayeswave_plot import find_trig as ft
import healpy as hp
import sky_area.sky_area_clustering as sky
import getopt
from bayeswave_plot.readbwb import BwbParams
import math
from optparse import OptionParser

from glue.ligolw import ligolw
from glue.ligolw import lsctables
from glue.ligolw import table
from glue.ligolw import utils

class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass


lsctables.use_in(LIGOLWContentHandler)


import lalinference.io.fits as lf
import lalinference.plot as lp
import lalinference.plot.cmap
def parser():
    """
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = OptionParser()
    parser.add_option("-M", "--mdc", type=str, default=None)
    parser.add_option("-N", "--nside", type=int, default=128)
    parser.add_option("-I", "--inj", type=str, default=None, help='Injection xml')
    parser.add_option("-e", "--eventnum", type=int, default=None, help='Injection event id')
    parser.add_option("-r", "--ra", type=float, default=None)
    parser.add_option("-d", "--dec", type=float, default=None)
    parser.add_option("--geo", default=False, action="store_true")
    
    (opts,args) = parser.parse_args()

    if len(args)==0:
        print >> sys.stderr, "ERROR: require BW directory"
        sys.exit()
    if not os.path.isdir(args[0]):
        print >> sys.stderr, "ERROR: BW dir %s does not exist"%args[0]
        sys.exit()


    return opts, args,

def gps_to_mjd(gps_time):
    """
    Convert a floating-point GPS time in seconds to a modified Julian day.

    Parameters
    ----------

    gps_time : float
        Time in seconds since GPS epoch

    Returns
    -------

    mjd : float
        Modified Julian day

    Example
    -------

    >>> '%.9f' % round(gps_to_mjd(1129501781.2), 9)
    '57316.937085648'
    """
    gps_seconds_fraction, gps_seconds = math.modf(gps_time)
    mjd = lal.ConvertCivilTimeToMJD(lal.GPSToUTC(int(gps_seconds)))
    return mjd + gps_seconds_fraction / 86400.





def gps_to_iso8601(gps_time):
    """
    Convert a floating-point GPS time in seconds to an ISO 8601 date string.

    Parameters
    ----------

    gps : float
        Time in seconds since GPS epoch

    Returns
    -------

    iso8601 : str
        ISO 8601 date string (with fractional seconds)

    Example
    -------

    >>> gps_to_iso8601(1000000000.01)
    '2011-09-14T01:46:25.010000'
    >>> gps_to_iso8601(1000000000)
    '2011-09-14T01:46:25.000000'
    >>> gps_to_iso8601(1000000000.999999)
    '2011-09-14T01:46:25.999999'
    >>> gps_to_iso8601(1000000000.9999999)
    '2011-09-14T01:46:26.000000'
    >>> gps_to_iso8601(1000000814.999999)
    '2011-09-14T01:59:59.999999'
    >>> gps_to_iso8601(1000000814.9999999)
    '2011-09-14T02:00:00.000000'
    """
    gps_seconds_fraction, gps_seconds = math.modf(gps_time)
    gps_seconds_fraction = '{0:6f}'.format(gps_seconds_fraction)
    if gps_seconds_fraction[0] == '1':
        gps_seconds += 1
    else:
        assert gps_seconds_fraction[0] == '0'
    assert gps_seconds_fraction[1] == '.'
    gps_seconds_fraction = gps_seconds_fraction[1:]
    year, month, day, hour, minute, second, _, _, _ = lal.GPSToUTC(int(gps_seconds))
    return '{0:04d}-{1:02d}-{2:02d}T{3:02d}:{4:02d}:{5:02d}{6:s}'.format(year, month, day, hour, minute, second, gps_seconds_fraction)





# -------------------------------------------
# Module to make skymaps, skyview webpage, etc.
# Works with single output directory from 
# BayesWaveBurst
# -------------------------------------------
def make_skyview(directory='.', mdc=None, NSIDE=128, inj=None, npost=5000):

    # -- Close any open figures
    plt.close('all')
    # ----------------
    # Read S6 MDC log file
    # -----------------
    if mdc is None:
        print "No MDC log provided."

    # -- Save PWD for future reference
    topdir = os.getcwd()

    # -- Enter requested directory
    os.chdir(directory)
    print "Entered", os.getcwd()

    try:
        jobname = 'bayeswave.run'
        bayeswave = open(jobname,'r')
    except:
        sys.exit("\n *bayeswave.run not found! \n")
    
    # ---------------------------------------
    # Extract information from bayeswave.run
    # Note: We may only need the trigger time from this
    # ---------------------------------------
#bayeswave = open(jobname, 'r')
    ifoNames = []
    for line in bayeswave:
        spl = line.split()
        if len(spl) < 1: continue
        
        # Determine names of IFOs and MDC scale factor
        if spl[0] == 'Command' and spl[1] == 'line:':
            for index, splElement in enumerate(spl):
                if splElement == '--ifo':
                    ifoNames.append(spl[index+1]) 
                    
        del spl[-1]
        # Determine number of IFOs
        if ' '.join(spl) == 'number of detectors':
            spl = line.split()
            ifoNum = spl[3]
            continue         
        # Determine gps trigger time
        if ' '.join(spl) == 'trigger time (s)':
            spl = line.split()
            gps = spl[3]
            break
    bayeswave.close()

    # --------------------------------
    # Create skymap summary statistics
    # --------------------------------

    # -- Input skymap data
    print "Extracting RA/DEC samples"
    filename = './chains/' + 'signal_params_h0.dat.0'
    data = np.loadtxt(filename, unpack=True,usecols=(0,1,2))
    ralist = data[1]
    sin_dec = data[2]
    print "Total samples are {0}".format(ralist.size)

    # -- Remove burn in samples
    burnin = ralist.size/4
    ralist = ralist[burnin:]
    sin_dec = sin_dec[burnin:]
    print "After removing burn-in samples are {0}".format(ralist.size)

    declist = np.arcsin(sin_dec)
    thetalist = np.pi/2.0 - declist

    radec = np.column_stack((ralist, declist))

    if radec.shape[0] > npost:
        radec = np.random.permutation(radec)[:npost,:]

    kde = sky.ClusteredSkyKDEPosterior(radec)

    # -- Get the sidereal time
    print "Finding sidereal time"
    print "Using GPS {0}".format(gps)
    trigtime = float(gps)
    lalgps = lal.LIGOTimeGPS(trigtime)
    sidtime = GreenwichSiderealTime(lalgps, 0)
    sidtime = sidtime % (np.pi*2)

    # -- Get the injection location
    print "GOT MDC?"
    print mdc
    if mdc is None:
        injtheta = 0
        injphi   = 0
        injdec   = 0
        injra    = 0
    else:
        injtheta, injphi = mdc.get_theta_phi(trigtime)
        injra = injphi + sidtime
        injdec = np.pi/2 - injtheta
        print "GOT INJECTION PARAMTERS"
        print injtheta, injra

    # -- Make plots directory, if needed
    plotsDir = './plots'
    if not os.path.exists(plotsDir):
        os.makedirs(plotsDir)

 
    # -- Plot the skymap and injection location
    skymap = kde.as_healpix(NSIDE, nest=True)

   
    fig = plt.figure(figsize=(8,6), frameon=False)
    ax = plt.subplot(111, projection='astro hours mollweide')
    ax.cla()
    ax.grid()
    lp.healpix_heatmap(skymap, nest=True, vmin=0.0, vmax=np.max(skymap), cmap=plt.get_cmap('cylon'))


    # TODO: our .png skymaps don't work anymore. 
    if (inj is not None):
        plt.plot(inj['ra'], inj['dec'], 'kx', ms=30, mew=1)
    if mdc is not None:
        plt.plot(injra, injdec, 'kx', ms=30, mew=1)
    plt.savefig(plotsDir+'/skymap.png')
    plt.close()



    # Meg's hacky way to get sky maps to work, because this:  lf.write_sky_map('skymap_{0}.fits'.format(gps), skymap, nest=True, gps_time=trigtime) doesn't work anymore
    skymap = np.atleast_2d(skymap)
    extra_header = []

    extra_header.append(('DATE-OBS', gps_to_iso8601(trigtime), 'UTC date of the observation'))
    extra_header.append(('MJD-OBS', gps_to_mjd(trigtime), 'modified Julian date of the observation'))

    hp.write_map('skymap_{0}.fits'.format(gps), skymap, nest=True, fits_IDL=True, coord='C', column_names=('PROB', 'DISTMU', 'DISTSIGMA', 'DISTNORM')[:len(skymap)], column_units=('pix-1', 'Mpc', 'Mpc', 'Mpc-2')[:len(skymap)], extra_header=extra_header)


    # -- Calculate the 50 and 90% credible intervals
    sq_deg = (180.0/np.pi)**2
    print "Calculating sky area ..."
    (area50, area90) = kde.sky_area( [0.5, 0.9] )*sq_deg
    print "Got area50 of {0}".format(area50)

    # -- Get the found contour and searched areas
    if mdc is not None:
        print "Calculating p value of injection"
        injcontour = kde.p_values( np.array([[injra, injdec]]) )[0]
        print "Got contour of {0}".format(injcontour)
    
        print "Calculating searched area..."
        searcharea = (kde.searched_area( np.array([[injra, injdec]]) )*sq_deg)[0]
        print "Got searched area of {0}".format(searcharea)

    elif (inj is not None):
        print "Calculating p value of command line injection"
        injcontour = kde.p_values( np.array([inj['ra'], inj['dec']]) )[0]
        print "Got contour of {0}".format(injcontour)

        print "Calculating searched area..."
        searcharea = (kde.searched_area( np.array([inj['ra'], inj['dec']]) )*sq_deg)[0]
        print "Got searched area of {0}".format(searcharea)

    else:
        injcontour = 0
        searcharea = 0
    pixarea = hp.nside2pixarea(NSIDE, degrees=True)

    # -- Make an output file with key statistics
    outfile = open('skystats.txt', 'w')
    outfile.write("# area50  area90  searcharea  injcontour \n")
    outfile.write("{0} {1} {2} {3}".format(area50, area90, searcharea, injcontour))
    outfile.close()


# -- Write main script for command line running
if __name__ == "__main__":

    # Example command line function calls
    # megasky.py /path/to/working/dir --mdc=/path/to/mdc/mdclog.txt
    # megasky.py --mdc=/path/to/mdc/mdclog.txt

    # Allow navigation into specified working directory
    topdir=os.getcwd()

    # Working directory is current directory unless it is specified by the 
    # first argument of the function call
    opts, args = parser()
         
    ra = opts.ra
    dec = opts.dec
    geo = opts.dec
    mdc = opts.mdc
    NSIDE = opts.nside

    injpos = None
    # Checl if injection file is present
    if opts.inj is not None:
      # If present, check if event id is provided
      # If yes, proceed. If no, throw error message and exit
      if(opts.eventnum is None):
        print >> sys.stderr, "Provide event num if giving injfile"
        sys.exit()
      print "Loading xml"    
      xmldoc = utils.load_filename(
           opts.inj, contenthandler=LIGOLWContentHandler)
      try:
        print('Checking if using a sim_inspiral table...')
        injs = table.get_table(
            xmldoc, lsctables.SimInspiralTable.tableName)
        inj = injs[opts.eventnum]
        injpos = {
            'ra': inj.longitude,
            'dec': inj.latitude,
            'id': inj.simulation_id}
        print(' yes')
      except:
        print('Checking if using a sim_burst table...')
        injs = table.get_table(xmldoc, lsctables.SimBurstTable.tableName)
        inj = injs[opts.eventnum]
        injpos = {'ra': inj.ra, 'dec': inj.dec, 'id': inj.simulation_id}
        print(' yes')
    
    # If ra and dec provided manually, use those
    if ra is not None and dec is not None:
      injpos  ={'ra': ra, 'dec':dec, 'id':0}
    # If md log provided, use that. Currently,
    # there is no scheme to assert exclusivity of injection and mdcs
    if mdc is not None:
      print 'Reading mdc log {0}'.format(mdc)
      try:
        mdc = ft.Mdc(arg)
      except:
        print 'Warning! Failed to read mdc'
        mdc = None

    
    make_skyview(args[0], mdc, NSIDE, injpos)

    # Move back to original dir
    os.chdir(topdir)


